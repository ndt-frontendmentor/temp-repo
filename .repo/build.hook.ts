import { copy } from 'https://deno.land/std@0.213.0/fs/mod.ts'
import { throwerr } from './_util.ts'

import 'https://deno.land/std@0.213.0/dotenv/load.ts'


// Feel free to modify this
export default async function build_challenge(name: string, dist_base: string)
{
   const src = `${name}/${Deno.env.get('DIST') || 'src'}`

   try
   {
      const { isDirectory } = await Deno.stat(src)
      isDirectory && copy(src, `${dist_base}/${name}`, { preserveTimestamps: true })
   }
   catch (e)
   {
      console.error(e)
      throwerr('Missing the `src` directory!', 1)
   }
}