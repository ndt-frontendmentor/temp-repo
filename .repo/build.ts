#!/usr/bin/env -S deno run --allow-read --allow-write --allow-env

import { build } from './build.util.ts'
import build_challenge from './build.hook.ts'

import { parseArgs } from 'https://deno.land/std@0.213.0/cli/parse_args.ts'

import 'https://deno.land/std@0.213.0/dotenv/load.ts'


const [ dirname ] = parseArgs(Deno.args)._?.[ 0 ].toString()

build({
   challenge_dir_name: dirname,
   dist_path: Deno.env.get('DIST')!,
   build_challenge,
})