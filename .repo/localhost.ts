#!/usr/bin/env -S deno run --allow-read --allow-env --allow-net

import { ServeDirOptions, serveDir } from 'https://deno.land/std@0.213.0/http/file_server.ts'

import 'https://deno.land/std@0.213.0/dotenv/load.ts'

const opt: ServeDirOptions =
{
   fsRoot: Deno.env.get('DIST'),
   urlRoot: Deno.env.get('BASE'),
   showDirListing: true,
   showIndex: true,
}

const serve_init: Deno.ServeOptions =
{
   // Why 0.0.0.0? https://stackoverflow.com/a/61676302
   hostname: '0.0.0.0',
   port: parseInt(Deno.env.get('PORT') || '8080')
}

Deno.serve(serve_init, req => serveDir(req, opt))

console.log(`Start from here » http://localhost:${port}/${opt.urlRoot}`)