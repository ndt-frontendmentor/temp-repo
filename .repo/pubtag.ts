#!/usr/bin/env -S deno run --allow-read --allow-write --allow-run --allow-env

import { build } from './build.util.ts'
import build_challenge from './build.hook.ts'

import { parseArgs } from 'https://deno.land/std@0.213.0/cli/parse_args.ts'
import { git } from './_util.ts'

import 'https://deno.land/std@0.213.0/dotenv/load.ts'
import { must_answer } from './_util.ts'


/** Main */
const [ tag, msg ] = get_input()

// 1. Build all challenges
await build({
   dist_path: Deno.env.get('DIST')!,
   build_challenge
})

// 2. Create annotated tag
await git([ 'tag', '-a', tag, '-m', `'${msg}'` ])

// 3. Push tag
await git([ 'push', '-u', 'origin', tag ])


/** Utilities */
type UserInput = [ `v${string}`, string ]

function get_input(): UserInput
{
   const args = parseArgs(Deno.args)._
   if (args.length === 2) return [ `v${args[ 0 ]}`, args[ 1 ].toString() ]

   return ask_user(args[ 0 ]?.toString())
}

function ask_user(suggested_tag = '1.0'): UserInput
{
   const tag = must_answer(
      'What is the version this time? (no "v" prefix)',
      suggested_tag
   )
   let log = `  v${tag}`
   console.log(log)

   const msg = prompt('Any note for this tag?', `version v${tag}`) || `version v${tag}`
   log += `: ${msg}`
   console.log(log)

   return [ `v${tag}`, msg as string ]
}