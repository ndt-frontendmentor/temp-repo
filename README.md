# Template Repository for Frontend Mentor Challenges

This is a template repo. I built this to share my pain from creating too many FM
challenges.

**What is Frontend Mentor** (FM)?

> Our professionally designed challenges help you gain hands-on experience
> writing HTML, CSS, and JavaScript. We create the designs so you can focus on
> the code and see your skills skyrocket!
>
> https://www.frontendmentor.io

## Requirement

This repo is designed to work in Linux environment. Probably it can work on
other OS implement UNIX standard as long as it has there tools:

1. [Deno](https://deno.land)
2. [Git](https://git-scm.com)

It doesn't get test on Windows and MacOS yet, so I would appreciated if you guys
could share me your experience.

### Onetime Setup

**Please remove this section once you are done**

1. Update placeholder in
   - section: [About Me](#about-me) (replace my info with your)
   - file: [LICENSE](./LICENSE)
2. Update Environment file: [`.env`](./.env)
   - `BASE`: (**REQUIRED**) Your remote repo slug, it's used by `new` script to
     generate your **Solution URL** and **Live Site URL**.
   - `DIST`: For renaming the distributing directory (default is `public` as
     Gitlab demand)
   - `TMPL`: For renaming the template directory name as you wish
   - `SRC`: For renaming the source directory in template directory; if you do
     this, don't forget to rename
     [the one in template directory](./.challenge-template/src) as well.
   - `PORT`: localhost port
3. Update [README template](./.challenge-template/README.md)
   - The file supports these 3 variables:
     - `$FMname`: FrontendMentor challenge name
     - `$FMurl`: FrontendMentor challenge URL
     - `$solutionPath`: it is the directory name
   - Update [Links](./.challenge-template/README.md#links) section:
     - If you use **Gitlab page**: replace `ndt-frontendmentor` with your Gitlab
       repo namespace
     - otherwise: try to use 3 variables to make yourself 2 good links
4. Update [`build.hook.ts`](./.repo/build.hook.ts) file
   - require TypeScript knownledge (or throw `any` everywhere if you wish)
   - the default hook is a simple task of copy all the files in `src` to
     `public` directory. You can do better with your skill and your imagination.
5. Update/Remove `.vscode`, please update it to match your own taste.

**Setup Section END: Please remove this if you are ready to push**

## Instruction

### Start new challenge

The script will download source challenge and mixes with template directory. All
you need is to give it correct slug name. You can found it in any public
challenge URL.

```shell
make new p=challenge-slug
```

For example, I plan to do this challenge
`https://www.frontendmentor.io/challenges/recipe-page-KiTsR8QQKm`. Therefore,
the challenge-slug will be `recipe-page`.

If you ever want to do another solution for a submitted challenge (make a
variant solution), you can use `v` flag:

```shell
make new p=challenge-slug v=another-solution
```

### Build a challenge

To build a specific challenge, run

```shell
make build c=challenge-slug
```

To build all, which will **overwrite** all the old builts, run

```shell
make build
```

### Local testing/debug

The repo comes with a super simple server that can serve static files from
`public/` directory. Just run the command below:

```shell
make dev
```

### Deployement

This repo is made to do very simple deploy on Gitlab. If you want to use it on
other source code hosting like Github. You will need to modify a bit. As for
Gitlab users, all you have to do is submit a new tag, the CI/CD system will
automatic copy all the files in `public/` and publish it to their Gitlab Page.

Here is step by step guide for deploying on **Gitlab**:

1. Build all the challenge you want to publish
2. Create a tag in localhost, recommend annotation tag:
   `git tag -a v1.0 -m 'version 1.0'`
3. Double check on everything before it too late :D
4. Push tag: `git push -u origin v1.0`

**OR do this:**

Create & PUBLISH tag with this comment:

- `make pubtag` → then enter tag name & desc → wait
- `make pubtag t=1.0 d="version 1.0"` → wait

## About Me

- Website - [Nguyễn Đăng Tú][ln-site]
- Frontend Mentor - [@ngdangtu-vn][ln-frontendmentor]\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone][ln-mastodon]
- **mastondon is optional, it represent for social network. replace it with your
  choice more if you want**

[ln-site]: https://ngdangtu.com
[ln-frontendmentor]: https://www.frontendmentor.io/profile/ngdangtu-vn
[ln-mastodon]: https://tilde.zone/@ngdangtu

## This repository License

<!-- Please do not modify this part. Thank you very much -->

```
ISC License

Copyright (c) 2024 Nguyễn Đăng Tú <biz@ngdangtu.dev>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
```

## Acknowledgments

<!-- You can add more credit for other people/projects help you in making your repo awesome again? -->

<!-- Please kindly do not remove the line below. Thank you very much :) -->

This repo is created base on the
[Template Repository for Frontend Mentor Challenges](https://gitlab.com/ndt-frontendmentor/temp-repo)
created by [Đăng Tú (ngdangtu)](https://ngdangtu.com)
